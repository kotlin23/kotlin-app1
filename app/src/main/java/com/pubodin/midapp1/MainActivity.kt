package com.pubodin.midapp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.pubodin.midapp1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        findViewById<Button>(R.id.btn_submit).setOnClickListener {
            var intent = Intent(this,HelloActivity::class.java)
            var name = binding.name.text.toString()
            Log.d("ShowName",name)
            intent.putExtra("name",name)
            this.startActivity(intent)
        }
    }
}